const socket = io("http://localhost:3223");
const mgsText = document.querySelector("#msg");
const btnSend = document.querySelector("#btn-send");
const chatBox = document.querySelector(".chat-content");
const displayMes = document.querySelector(".message");
const userName = document.querySelector("#user-name");

const display = (msg, type) => {
    const msgDiv = document.createElement("div");
    let className = type;
    msgDiv.classList.add(className, "message-row");
    let time = new Date().toLocaleDateString();

    let innerText = `
      <div class="message-title">
          <span>${msg.name}</span>
      </div>
      <div class="message-text">${msg.message}</div>
      <div class="message-time">
        ${time}
      </div>
  `;
    msgDiv.innerHTML = innerText;
    displayMes.appendChild(msgDiv);
};
const sendMsg = (message) => {
    let msg = {
        name: name,
        message: message.trim(),
    };
    display(msg, "my-message");
    socket.emit("sendMsg", msg);
};

let name;
do {
    name = prompt("Enter your name: ");
} while (!name);
mgsText.focus();

userName.textContent = name;

btnSend.addEventListener("click", (e) => {
    e.preventDefault();
    sendMsg(mgsText.value);
    mgsText.value = "";
    mgsText.focus();
});

socket.on("sendToClient", (msg) => {
    display(msg, "other-message");
});