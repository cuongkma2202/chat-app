const express = require("express");
const { engine } = require("express-handlebars");
const { createServer } = require("http");
const path = require("path");
const { Server } = require("socket.io");

const app = express();
app.engine(".hbs", engine({ extname: ".hbs" }));
app.set("view engine", ".hbs");
app.set("views", "./src/views");
app.use(express.static(path.join(__dirname, "public")));
console.log(__dirname);
const http = createServer(app);
const io = new Server(http);
io.on("connection", (socket) => {
    console.log("connected");

    socket.on("sendMsg", (msg) => {
        socket.broadcast.emit("sendToClient", msg);
    });
});
app.get("/", (req, res) => {
    res.render("home");
});

const PORT = 3223;
http.listen(PORT, () => {
    console.log(`APP RUNNING ON PORT ${PORT}`);
});